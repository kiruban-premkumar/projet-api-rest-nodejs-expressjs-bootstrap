
/**
 * Module dependencies.
 */

var server_host = "http://localhost:3000";
var express = require('express');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


/**********************
	INITIAL DATA
**********************/

var data = require('./films.json');
var movies = data['films'];


/**********************
	API (SERVER)
**********************/

// Movies Listing
app.get('/films', function(req,res){
	res.setHeader('Content-Type', 'application/json; charset=utf-8');
	var keyword = req.query.search;
	var results = [];

	// Search results

	if(keyword)
	{
		for(var i = 0; i < movies.length; i++)
		{
			if(	movies[i].scenaristes.indexOf(keyword) != -1 || movies[i].realisateurs.indexOf(keyword) != -1 || movies[i].acteurs.indexOf(keyword) != -1 )
			{
				results.push({ '_id': i, 'titre': movies[i].titre, 'scenaristes': movies[i].scenaristes,
								'realisateurs': movies[i].realisateurs, 'acteurs': movies[i].acteurs, 'date_de_sortie': movies[i].date_de_sortie,
								'langue': movies[i].langue, 'genre': movies[i].genre});
			}
		}
	}
	else
	{
		for(var i = 0; i < movies.length; i++)
		{
			results.push({ '_id': i, 'titre': movies[i].titre, 'scenaristes': movies[i].scenaristes,
							'realisateurs': movies[i].realisateurs, 'acteurs': movies[i].acteurs, 'date_de_sortie': movies[i].date_de_sortie,
							'langue': movies[i].langue, 'genre': movies[i].genre});
		}
	}

	// Pagination with next / previous links

	var current_page = req.query.page?req.query.page:1;
	var per_page = req.query.per_page?req.query.per_page:3; // 3 films par page par défaut...
	var offset = (current_page - 1) * per_page;

	var next_page = results.length > offset + per_page ? '?page='+(parseInt(current_page)+1) : null;
	if(next_page && keyword)
	{
		next_page += '&search='+keyword;
	}

	var previous_page = current_page > 1 ? '?page='+(parseInt(current_page)-1) : null;
	if(previous_page && keyword)
	{
		previous_page += '&search='+keyword;
	}
	
	var current_page_movies = [];
	for(var j = offset; j < offset + per_page && j < results.length ; j++)
	{
		current_page_movies.push(results[j]);
	}

    res.end(JSON.stringify({'films': current_page_movies, '_links': {'previous': previous_page,'next': next_page}}));
});

// A Movie Description
app.get('/films/:id', function(req,res){
	var id = req.params.id;
	current_movie = movies[id];

	// GET the POSTER
	var url = 'http://www.omdbapi.com/?t='+current_movie.titre+'&plot=full&r=json';

	http.get(url, function(omdb_res) {
	    var body = '';

	    omdb_res.on('data', function(chunk) {
	        body += chunk;
	    });

	    omdb_res.on('end', function() {
	    	// GET the Trailer
	    	var yt_url = "http://gdata.youtube.com/feeds/api/videos?q="+current_movie.titre+"%20bande%20annonce&max-results=1&v=2&alt=jsonc&orderby=viewCount";
	        http.get(yt_url, function(yt_res) {
			    var yt_body = '';

			    yt_res.on('data', function(yt_chunk) {
			        yt_body += yt_chunk;
			    });

			    yt_res.on('end', function() {
			    	var ytResponse = JSON.parse(yt_body);
			        var omdbResponse = JSON.parse(body);
			        var poster = omdbResponse.Poster;
			        //var trailer = ytResponse.data.items[0].player.default;
			        var id_youtube = ytResponse.data.items[0].id;
			        var trailer = 'https://www.youtube.com/embed/'+id_youtube;
			        //console.log(ytResponse.data.items[0].id);
			        var ret = {'_id': id, 'titre': current_movie.titre, 'synopsis': current_movie.synopsis, 'scenaristes': current_movie.scenaristes,
						'realisateurs': current_movie.realisateurs, 'acteurs': current_movie.acteurs, 'date_de_sortie': current_movie.date_de_sortie,
						'langue': current_movie.langue, 'genre': current_movie.genre, 'trailer': trailer, 'poster': poster};
					res.setHeader('Content-Type', 'application/json; charset=utf-8');
		    		res.end(JSON.stringify(ret));
		    	});
			}).on('error', function(e) {
			      console.log("Got error: ", e);
			});

	    });
	}).on('error', function(e) {
	      console.log("Got error: ", e);
	});

});

// Recommendation based on a movie 
app.get('/films/:id/recommandations', function(req,res){
	var id = req.params.id;
	current_movie = movies[id];
	all_persons = current_movie.scenaristes.concat(current_movie.realisateurs,current_movie.acteurs);
	results = [];
	for(var i=0;i < movies.length && results.length < 2; i++)
	{
		for(var j = 0; j < all_persons.length; j++)
		{
			// we skip the current movie
			if(id == i) break;

			if( movies[i].scenaristes.indexOf(all_persons[j]) != -1 || movies[i].realisateurs.indexOf(all_persons[j]) != -1 || movies[i].acteurs.indexOf(all_persons[j]) != -1)
			{
				results.push({'_id': i,'titre': movies[i].titre,'description': server_host+'/films/'+i});
				break;
			}
		}
	}

	res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.end(JSON.stringify(results));
});

/**********************
	WEB PAGES (CLIENT)
**********************/

// @TODO
// Html Movies Listing 
app.get('/', function(req,res){

	var current_page = req.query.page?req.query.page:1;
	var url;

	if(req.query.search)
	{
		url = server_host + '/films?page='+current_page+'&search='+req.query.search;
	}
	else
	{
		url = server_host + '/films?page='+current_page;
	}

	http.get(url, function(res_films) {
	    var body = '';

	    res_films.on('data', function(chunk) {
	        body += chunk;
	    });

	    res_films.on('end', function() {
	    	var films = JSON.parse(body);
	    	//console.log(films);
			res.render('index', { title: 'Liste des films', data: films, current: current_page });
		});
	}).on('error', function(e) {
	      console.log("Got error: ", e);
	});

});

// @TODO
// Html Movie Description
app.get('/detail/:id', function(req,res){
	var id = req.params.id;
	var url = server_host + '/films/'+id;

	http.get(url, function(res_film) {
	    var body = '';

	    res_film.on('data', function(chunk) {
	        body += chunk;
	    });

	    res_film.on('end', function() {
	    	var film = JSON.parse(body);

	    	// films recommandés pour le film affiché
	    	var url_reco = server_host + '/films/'+id+'/recommandations';

	    	http.get(url_reco, function(res_reco) {
			    var body_reco = '';

			    res_reco.on('data', function(chunk) {
			        body_reco += chunk;
			    });

			    res_reco.on('end', function() {
			    	var recommandations = JSON.parse(body_reco);

			    	//console.log(recommandations);
					res.render('detail', { title: 'Film\'s Description', film: film, recommandations: recommandations });
				});
			}).on('error', function(e) {
			      console.log("Got error: ", e);
			});
		});
	}).on('error', function(e) {
	      console.log("Got error: ", e);
	});
});



http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


// http://www.omdbapi.com/?t=undefined&plot=full&r=json