
# PROJET SIA 2014

Auteurs : Kiruban PREMKUMAR & Marina TANG


Dans le cadre de ce projet nous utiliserons le framework express.

# Les ressources utiles

On a besoin d'une ressource FILMS.

- Le mot clé de la recherche se fera avec la méthode GET : attribut recherche.
- La pagination se fera avec la méthode GET aussi : attribut page.

## Exemples d'appel de la ressource FILMS :

- /films
- /films?page=1
- /films?recherche=keyword
- /films?recherche=keyword&page=1

## Pour résumer :

HTTP VERB       	| PATH         		| COMMENTAIRE  						
---------------- 	| ----------------	| -----------------------------
`GET`     		| /films 			| Liste des films					
`GET`      		| /films/:id      	| Description d'un film 			

## Tests avec curl :

...