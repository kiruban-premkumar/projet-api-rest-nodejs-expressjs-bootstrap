var markdownpdf = require("markdown-pdf")
  , fs = require("fs")

fs.createReadStream("RAPPORT.md")
  .pipe(markdownpdf())
  .pipe(fs.createWriteStream("rapport.pdf"))